---
# file: api-ansible/api-v3-php.yaml

# Description:
#   Builds out a PHP server, then deploys the api v3 code to it

# Preconditions:
#   (nothing crazy, git repo, etc.)

# Examples:

# ansible-playbook \
# api-v3-php.yaml \
# -i hosts/ec2.py \
# --user=ubuntu \
# --private-key=secrets/sage2.pem \
# --extra-vars "wfm_team=api wfm_environment=integration wfm_application=sagev3php wfm_misc_color=puce php_environment=integration git_tag=v3-integration" \
# --vault-password-file ~/.vault_pass.txt

# Example to only update code:
# ansible-playbook \
# api-v3-php.yaml \
# -i hosts/ec2.py --user=ubuntu --private-key=secrets/sage2.pem \
# --extra-vars="wfm_team=api wfm_environment=integration wfm_application=sagev3php wfm_misc_color=puce php_environment=integration git_tag=v3-integration" \
# --tags="update_code,always"

# TODOs:
#   Need smoke tests built for it
#   Need to handle the is_vagrant cases better so Packer can generate stuff
#   Python role refactor outta newrelic (see httplib2)
#   Need a way to identify the proper git_tag for vagrant environments, because
# that gets sent to rollbar.  Currently just sending "v3-integration"
#   Consider populating the Postgres database

# WARNINGS:
#   Removed variable defaults for "php_environment" and "git_tag" to reduce the chances of
#   deploying integration to an advanced lifecycle incorrectly
#
#   Support for the "always" meta-tag isn't included until Ansible 1.9
#   If you are restricting execution of a playbook to tag "foo", please manually
#   include the "always" tag, i.e. --tags=foo,always

- include: includes/playbooks/check-ansible-version.yaml
- include: includes/playbooks/check-hosts-variables.yaml

- name: "api-v3-php.yaml"

  hosts: '{{ ["tag_wfm_team_",wfm_team,":&","tag_wfm_environment_",wfm_environment,":&","tag_wfm_application_",wfm_application,":&","tag_wfm_misc_color_",wfm_misc_color]|join("") if ( (wfm_team is defined) and (wfm_environment is defined) and (wfm_application is defined) and (wfm_misc_color is defined) ) else hosts_tag }}' # "

  vars:
    wfm_pb_name: "api-v3-php.yaml"
    git_url: git@bitbucket.org:wfmdigitalweb/api-main-php.git
    git_dest: /usr/share/nginx/html/
    git_key_file: /var/www/.ssh/id_rsa
    nginx_user: www-data
    php_app_name: "sage-v3-php-{{ php_environment }}"
    is_vagrant: false
    # is_vagrant: "{{ansible_all_ipv4_addresses.1.find('192.168.88') != -1}}"
    test_rollbar: false

  pre_tasks:
    - include: includes/notify-sumo.yaml
      wfm_playbook: "{{ wfm_pb_name }}"
      wfm_state: "START"

    - name: Check for required variable php_environment
      fail: msg="Variable php_environment is not defined"
      when: php_environment is not defined
      tags: always

    - name: Check for required variable git_tag
      fail: msg="Variable git_tag is not defined"
      when: git_tag is not defined
      tags: always

    - name: Check for invalid value of variable wfm_application
      fail: msg="Variable wfm_application may be set incorrectly - expecting sagev3php"
      when: wfm_application is defined and wfm_application != 'sagev3php'
      tags: always

  roles:

  # 1 build servers
    - { role: common
      , tags: "buildstep, common"
      }
    - { role: nginx
      , tags: "buildstep,nginx"
      }
    - { role: php5
      , php5_is_dev: "{{ is_vagrant }}"
      , tags: "buildstep,php5"
      }
    - { role: mongoDB-26
      , tags: "buildstep, mongoDB"
      , when: is_vagrant
      }     # Install full mongo if local, for everyone else install php5-mongo
    - { role: DEVmongoData
      , mongodata_install_gzip: False
      , mongodata_create_users: True
      , tags: "buildstep,mongodata"
      , when: is_vagrant
      }
    - { role: newrelic
      , tags: "buildstep,newrelic"
      , when: not is_vagrant
      }
    - { role: rollbar_agent
      , rollbar_collectordir_owner: "{{ nginx_user }}"
      , rollbar_access_token: "{{ rollbar_keys[ 'php' ] }}"
      , rollbar_environment: "{{ php_environment }}"
      , rollbar_code_root: "{{ git_dest }}"
      , rollbar_code_branch: "{{ git_tag }}"
      , rollbar_dump_directory: '/usr/share/nginx/rollbar'
      , tags: "buildstep,rollbar_agent"
      , when: (not is_vagrant) or (test_rollbar)
      }
    - { role: sumologic
      , sumo_collector_config_file: 'sage-php-collector.json'
      , sumo_machine_id: 'sagev3'
      , sumo_machine_env: "{{ php_environment }}"
      , tags: ['sumologic']
      , when: not is_vagrant
      }
    - { role: ironcli
      , tags: ['ironcli']
      }
    - { role: postgres
      , postgres_database_name: upm-local
      , postgres_test_database_name: upm-test
      , postgres_user: apiadmin
      , postgres_test_user: apiadmintest
      , postgres_password: astronkpassw0rd
      , postgres_test_password: astronkpassw0rdtest
      , postgres_version: 9.4
      , postgres_global_peer_method: trust
      , tags: ['postgres']
      , when: is_vagrant
      # TODO - Add step to populate the database via phinx?
      # or leave that up to Vagrant / the user?
      }
    - { role: bennojoy.ntp
      , sudo: yes
      , tags: "ntp"
      , when: not is_vagrant
      }
#    - { role: devops-role-barricade
#      , barricade_license_key: "{{ default_barricade_key }}"
#      , tags: ['devops-role-barricade']
#      , when: not is_vagrant
#      }
    - { role: xdebug
      , tags: "buildstep, xdebug"
      , when: is_vagrant
      }
#    - { role: redis
#      , redis_is_development: "{{ is_vagrant | default(False) }}"
#      , tags: ['buildstep','redis']
#      }
# redis_is_development


  # 2 customize
    - { role: apply-v3-php
      , tags: "customizestep, apply_v3_php"
      }
    - { role: local-dev-box
      , local_dev_directories: ['/home/vagrant', '/root', '/var/www']
      , install_postgres_tools: true
      , tags: ['local-dev-box']
      , when: is_vagrant
      }

  # 3 deploy code
  tasks:

    - name: All IP4 addresses
      debug: var=ansible_all_ipv4_addresses

    - name: 0th IP4 address
      debug: var=ansible_all_ipv4_addresses.0

    - name: 1th IP4 address
      debug: var=ansible_all_ipv4_addresses.1

    - name: Is Vagrant?
      debug: var=is_vagrant

    - name: Install php5 mongo lib
      apt: pkg=php5-mongo state=present update_cache=false
      sudo: yes
      when: inventory_hostname_short != 'vagrant_img_app'
      tags:
        - update_code

    - name: Add Bitbucket to known_hosts to prevent composer and git timeout
      known_hosts:
        path='/var/www/.ssh/known_hosts'
        host='bitbucket.org'
        state=present
        key="bitbucket.org ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw=="
      sudo: True
      sudo_user: "{{ nginx_user }}"
      tags:
        - update_code

  # TODO: determine if this include needs to be skipped for vagrant installs, and if
  # so, skip it in a way that doesn't break Packer
    - include: includes/update-code.yaml
      tags: update_code
      when: not is_vagrant

    - name: Ensure cache and log ramdisk exists
      file: path={{ item }} state=directory owner="{{ nginx_user }}" group="{{ nginx_user }}" mode=0775
      sudo_user: "{{ nginx_user }}"
      sudo: True
      with_items:
        - /dev/shm/cache
        - /dev/shm/logs
        - /var/www/.ssh
      tags:
        - update_code

    - name: Ensure vagrant users can change user to the nginx user to run tests
      user:
        name="{{ nginx_user }}"
        shell="/bin/bash"
      sudo: True
      tags:
        - update_code
      when: is_vagrant

    - include_vars: secrets/github-mi-six-token.yaml

    - name: Add github token to composer
      shell: composer config -g github-oauth.github.com {{ github_reader_token }}
      no_log: True


    - name: Update composer packages
      #      shell: COMPOSER_PROCESS_TIMEOUT=2000 composer install --no-interaction
      shell: composer install --no-interaction
      sudo: True
      sudo_user: "{{ nginx_user }}"
      args:
        chdir: /usr/share/nginx/html
      tags:
        - update_code

    - name: Optimize Composer
      sudo: yes
      shell: composer dump-autoload --optimize
      sudo: True
      sudo_user: "{{ nginx_user }}"
      args:
        chdir: /usr/share/nginx/html
      tags:
        - update_code

    - name: Clear cache
      sudo: True
      sudo_user: "{{ nginx_user }}"
      shell: php /usr/share/nginx/html/app/console cache:clear --env={{ php_environment }}
      tags:
        - update_code

    - name: Clear sandbox cache for twig documentation things
      sudo: True
      sudo_user: "{{ nginx_user }}"
      shell: php /usr/share/nginx/html/app/console cache:clear --env=sandbox
      tags:
        - update_code

    - name: Warmup cache
      sudo: True
      sudo_user: "{{ nginx_user }}"
      shell: php /usr/share/nginx/html/app/console cache:warmup --env={{ php_environment }}
      tags:
        - update_code

    - name: Force handlers
      debug: msg="Forcing nginx and php5-fpm handlers"
      changed_when: true
      notify:
        - restart nginx
        - restart php5-fpm
      tags:
        - update_code

    - name: Restart web server
      sudo: True
      service: name={{ item.name }} state=restarted
      with_items:
        - { name: "nginx" }
        - { name: "php5-fpm" }
      tags:
        - "update_code, manual_restart"

  post_tasks:
    - include: includes/notify-sumo.yaml
      wfm_playbook: "{{ wfm_pb_name }}"
      wfm_state: "END"
      tags: always

    - name: Debug 1
      debug: msg="Post task before includes"
      tags: always

    # Run Codeception when IS_VAGRANT
    - name: Codeception tests
      sudo: True
      sudo_user: "{{ nginx_user }}"
      args:
        chdir: "/usr/share/nginx/html"
      shell: "./bin/codecept run"
      tags:
        - cests
      when: is_vagrant

    # Notify our services when NOT IS_VAGRANT

    - include: includes/notify-hipchat.yaml what_deployed={{ php_app_name }} what_deployed_version={{ git_tag }}
      tags: always
      when: not is_vagrant
    - include: includes/notify-rollbar.yaml project_rollbar_key={{ rollbar_keys[ 'php' ] }} rollbar_notifier='sage-ansible-deployer' node_environment={{ php_environment }}
      tags: always
      when: (not is_vagrant) or (test_rollbar)
    - include: includes/notify-new-relic.yaml
        nrd_app_name={{ php_app_name }}
        nrd_user="Ansible - Build"
        nrd_revision={{ git_tag }}
      tags: always
      when: not is_vagrant

    - name: Delay one minute before running smoke tests
      pause: minutes=1
      tags: always
      when: not is_vagrant

#    - include: includes/smoke-ec2appserver.yaml
#      tags: always
#      when: not is_vagrant

    - include: includes/smoke-appserver.yaml
      tags: always
      when: not is_vagrant

    - include: includes/smoke-sage-php.yaml
      tags: always
      when: not is_vagrant

    # Notify again if smoke tests pass
    - include: includes/notify-hipchat.yaml
        what_deployed="{{ php_app_name }} Smoke Tests"
        what_deployed_version="{{ git_tag }}"
      tags: always
      when: not is_vagrant

    - name: Debug 2
      debug:
        msg="Post task After includes"
      tags: always
