---
# file: api-ansible/ec2-launch-autocolor-instance.yaml

# Description:
#   Attempt to launch ec2 instances for the Sage/API Team
#   Grabs the color info created by ec2-capture-app-color.yaml

# Tower Jobs:
#   (none yet)

# Preconditions:
#   export AWS_ACCESS_KEY_ID=xxx
#   export AWS_SECRET_ACCESS_KEY=xxx
#   /etc/ansiblebs/FILE exists

# Examples:
#   ansible-playbook \
#   ec2-launch-autocolor-instance.yaml \
#   -i hosts/ec2.py \
#   --extra-vars "hosts_tag=autocolor instance_count=2 instance_type=m3.medium wfm_team=api wfm_environment=integration wfm_application=sagev2node" \
#   --vault-password-file ~/.vault_pass.txt

# TODOs:
#   Fix "report instances to user" to check for id before printing to avoid errors
#   Need to automate adding elastic ip to newly launched server
#   Make vars: image: a global variable since it is a standard (BEWARE: make sure it works on Tower)

# WARNINGS:
#   count_tag is used to limit number of servers launched.
#   It is now a conjunction of team, environment, app, and color tagging instead of a string

- include: includes/playbooks/check-ansible-version.yaml

- name: Launch/configure app server instance
  hosts: 127.0.0.1
  connection: local

  vars:
    wfm_pb_name: "ec2-launch-autocolor-instance.yaml"
    pb_required_vars:
      - wfm_team          # wfm:team tag for ec2 i.e. api, devops
      - wfm_environment   # wfm:environment tag for ec2 i.e. integration
      - wfm_application   # wfm:application tag for ec2 i.e. sagev2xxx
    instance_type: "m3.medium"   #m3.xlarge for production
    region: us-east-1
    zone: us-east-1d
    vpc_subnet_id: "{{ vpc_subnet_id }}" # Subnet ID inside of a VPC
    image: "{{ wfm_api_preferred_ami }}" # ubuntu 14.04 : us-east-1, 64-bit, instance root store
    sage_aws_group: "{{ wfm_sage_app_sg[wfm_application] }}" # Security group; very important
    instance_count: 1 # Default to a single instance, user may override

  pre_tasks:
    - include: includes/notify-sumo.yaml
      wfm_playbook: "{{ wfm_pb_name }}"
      wfm_state: "START"
    - include: includes/check-pb-required-vars.yaml
    - set_fact:
        wfm_precolor_string: "{{wfm_team+'_'+wfm_environment+'_'+wfm_application}}"
    # - debug:
    #     msg="{{wfm_precolor_string}}"
    # - debug:
    #     msg="{{'/etc/ansiblebs/'+wfm_precolor_string+'_oldcolor.txt'}}"
   - stat:
        path="{{'/etc/ansiblebs/'+wfm_precolor_string+'_oldcolor.txt'}}"
       register: oldcolorfile_stat
    - debug:
        var=oldcolorfile_stat
    - fail:
        msg="old color file might be unreadable, giving up!"
      when: (oldcolorfile_stat.stat.roth is not defined) or (not oldcolorfile_stat.stat.roth)
    - stat:
        path="{{'/etc/ansiblebs/'+wfm_precolor_string+'_oldcolor.txt'}}"
      register: newcolorfile_stat
    - fail:
        msg="new color file might be unreadable, giving up!"
      when: (newcolorfile_stat.stat.roth is not defined) or (not newcolorfile_stat.stat.roth)

    - set_fact:
        wfm_new_color: "{{ lookup('file', '/etc/ansiblebs/'+wfm_precolor_string+'_newcolor.txt') }}"

    - set_fact:
        sage_aws_servername: "{{ wfm_team }}:{{ wfm_environment }}:{{ wfm_application }}:{{ wfm_new_color }}"

  tasks:
    - name: debug
      debug:
        msg="{{sage_aws_servername}}"

    - name: Launch server
      local_action:
        module: ec2
        key_name: sage2
        group: "{{ sage_aws_group }}"
        instance_type: "{{ instance_type }}"
        image: "{{ image }}"
        region: "{{ region }}"
        zone: "{{ zone }}"
        vpc_subnet_id: "{{ vpc_subnet_id }}"
        wait: yes
        wait_timeout: 500
        exact_count: "{{ instance_count }}"
        count_tag:
          'wfm:team': "{{ wfm_team }}"
          'wfm:environment': "{{ wfm_environment }}"
          'wfm:application': "{{ wfm_application }}"
          'wfm:misc:color': "{{ wfm_new_color }}"
      register: ec2_instances

    - include: includes/build-ec2-inventory.yaml

    - name: Tag instances
      local_action: ec2_tag
                    resource={{ item.id }}
                    region={{ region }}
                    state=present
      with_items: ec2_instances.tagged_instances
      args:
        tags:
          Name: "{{ sage_aws_servername }}"
          "wfm:team": "{{ wfm_team }}"
          "wfm:environment": "{{ wfm_environment }}"
          "wfm:application": "{{ wfm_application }}"
          "wfm:misc:color": "{{ wfm_new_color }}"

    - name: wait for instances to listen on port:22
      wait_for: state=started
                host={{ item.public_dns_name }}
                port=22
                delay=60
                timeout=320
      with_items: ec2_instances.tagged_instances

    - name: Report instances to user
      debug:
        msg="Instance {{ item.id }} with public IP {{ item.public_ip }}"
      with_items: ec2_instances.instances

# Removed from add_host:
#        the private key file should be specified at CLI / Tower level
#        ansible_ssh_private_key_file='secrets/sage2.pem'

    - add_host:
        name={{ item.public_dns_name }}
        groups='autocolor'
        ansible_ssh_user='ubuntu'
        ansible_ssh_host={{ item.public_dns_name }}
        ec2_private_dns_name={{ item.private_dns_name }}
        ec2_id={{ item.id }}
        ec2_tag_Name={{ sage_aws_servername }}
      with_items: ec2_instances.instances

  post_tasks:
    - include: includes/notify-sumo.yaml
      wfm_playbook: "{{ wfm_pb_name }}"
      wfm_state: "END"
      tags: always

# - name: Test hitting dynamic inventory
#   hosts: "{{ hosts_tag }}"
#   tasks:
#     - name: Test
#       debug:
#         msg='Hello whirled'

#     - name: prune any unnecessary npm modules
#       command: npm prune
#       args:
#         chdir: /var/www/sage/v2
#       sudo: true
