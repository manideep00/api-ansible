#!/usr/bin/python

# See http://docs.ansible.com/developing_modules.html

# ansible/hacking/test-module \
# -m ./api-ansible/library/newrelic_server.py \
# -a "token=\"YOURTOKENHERE\" server_name=\"ip-xx-2xx-1xx-xx\""

# Adapted from the newrelic_deployment module which is Copyright 2013 Matt Coddington <coddington@gmail.com>
# See ansible/lib/ansible/modules/extras/monitoring/newrelic_deployment.py for GNU license, etc

# Removed requirements, added explicit urllib import to match recent changes

# TODOs:
#   Pass the info gained back so that playbooks can capture it and operate on it
#   Get opinions from devs on how to make it all cleaner, more feature-rich

DOCUMENTATION = '''
---
module: newrelic_server
version_added: "1.5.1"
author: WFM and Matt Coddington
short_description: Check newrelic to see if a server is reporting
description:
   - Check newrelic to see if a server is reporting (see https://docs.newrelic.com/docs/apm/apis/server-examples-v2/listing-your-server-id-metric-data-api-v2)
options:
  token:
    description:
      - API token.
    required: true
  server_name:
    description:
      - Name of the server to find; returned as name in the servers array by NR api
    required: true
  validate_certs:
    description:
      - If C(no), SSL certificates will not be validated. This should only be used
        on personally controlled sites using self-signed certificates.
    required: false
    default: 'yes'
    choices: ['yes', 'no']
    version_added: 1.5.1
# informational: requirements for nodes
requirements: [ ]
'''

EXAMPLES = '''
- newrelic_server: token=AAAAAA
                       server_name=ip-xxx-xxx-xxx
'''
import urllib

# ===========================================
# Module execution.
#

def main():

    module = AnsibleModule(
        argument_spec=dict(
            token=dict(required=True),
            server_name=dict(required=True),
            validate_certs = dict(default='yes', type='bool'),
        ),
        supports_check_mode=True
    )

    # build list of params
    params = {}
    params["filter[name]"]=module.params["server_name"]

    # If we're in check mode, just exit pretending like we succeeded
    if module.check_mode:
        module.exit_json(changed=True)

    # Send the data to NewRelic
    url = "https://api.newrelic.com/v2/servers.json"
    data = urllib.urlencode(params)
    headers = {
        'x-api-key': module.params["token"],
    }
    response, info = fetch_url(module, url, data=data, headers=headers) 

    # Look at the response
    goodserver = False
    badserverreason = ""

    if info['status'] != 200:
        module.fail_json(msg="unable to query newrelic: %s" % info['msg'])
    else:
        nrservers = json.load(response)

        if len(nrservers['servers']) == 0:
          badserverreason = "%s not found in New Relic" % module.params["server_name"]

        for nrserver in nrservers['servers']:
          if nrserver['name'] == module.params["server_name"]:
            if nrserver['reporting']:
              goodserver = True
            else:
              badserverreason = "Server reporting status %s" % nrserver['reporting']

        if goodserver: 
          module.exit_json(changed=False)
        else:
          module.fail_json(msg=badserverreason)
    

# import module snippets
from ansible.module_utils.basic import *
from ansible.module_utils.urls import *

main()